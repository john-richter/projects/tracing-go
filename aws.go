package tracing

import (
	"context"
	"fmt"
	"net/http"

	awsMiddleware "github.com/aws/aws-sdk-go-v2/aws/middleware"
	xrayHeader "github.com/aws/aws-xray-sdk-go/header"
	"github.com/aws/aws-xray-sdk-go/xray"
	smithyMiddleware "github.com/aws/smithy-go/middleware"
	smithyHttp "github.com/aws/smithy-go/transport/http"
	httptrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/net/http"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace"
	traceext "gopkg.in/DataDog/dd-trace-go.v1/ddtrace/ext"
)

const (
	tagAWSAgent     = "aws.agent"
	tagAWSOperation = "aws.operation"
	tagAWSRegion    = "aws.region"

	defaultAWSAgent = "aws-sdk-go-v2"
)

var (
	AwsHttpClient = httptrace.WrapClient(
		&http.Client{},
		httptrace.RTWithResourceNamer(awsResourceName),
		httptrace.WithBefore(injectSpanMetadata),
	)
)

func awsService(r *http.Request) string {
	return awsMiddleware.GetSigningName(r.Context())
}

func awsServiceName(r *http.Request) string {
	return "aws." + awsService(r)
}

func awsResourceName(r *http.Request) string {
	return fmt.Sprintf("%s.%s", awsService(r), awsOperationName(r))
}

func awsOperationName(r *http.Request) string {
	return awsMiddleware.GetOperationName(r.Context())
}

func awsRegion(r *http.Request) string {
	//
	// TODO: Not sure which one of these is correct
	//

	// return awsMiddleware.GetRegion(r.Context())
	return awsMiddleware.GetSigningRegion(r.Context())
}

func awsAgent(r *http.Request) string {
	if agent := r.Header.Get("User-Agent"); agent != "" {
		return agent
	}
	return defaultAWSAgent
}

func injectSpanMetadata(r *http.Request, span ddtrace.Span) {
	span.SetTag(traceext.ServiceName, awsServiceName(r))
	span.SetTag(tagAWSAgent, awsAgent(r))
	span.SetTag(tagAWSOperation, awsOperationName(r))
	span.SetTag(tagAWSRegion, awsRegion(r))
}

//
// Middleware for the AWS SDK to parse, generate, and propogate AWS Trace ID headers.
// Could probably do this in the injectSpanMetadata function instead.
//

type AwsApiOption = func(*smithyMiddleware.Stack) error

type addAwsXrayTraceHeadersMiddleware struct{}

func (m *addAwsXrayTraceHeadersMiddleware) ID() string { return "AddAwsXrayTraceHeaders" }

func (m *addAwsXrayTraceHeadersMiddleware) HandleBuild(
	ctx context.Context,
	input smithyMiddleware.BuildInput,
	handler smithyMiddleware.BuildHandler,
) (output smithyMiddleware.BuildOutput, metadata smithyMiddleware.Metadata, err error) {
	r, ok := input.Request.(*smithyHttp.Request)
	if !ok {
		return output, metadata, fmt.Errorf("unknown transport type %T", input.Request)
	}
	if traceHeaderIfce := ctx.Value(xray.LambdaTraceHeaderKey); traceHeaderIfce != nil {
		traceHeaderValue, ok := traceHeaderIfce.(string)
		if !ok {
			return output, metadata, fmt.Errorf("unknown xray trace header type %T", traceHeaderIfce)
		}
		traceHeader := xrayHeader.FromString(traceHeaderValue)
		if traceHeader == nil {
			traceHeader = &xrayHeader.Header{}
		}
		if len(traceHeader.TraceID) == 0 {
			traceHeader.TraceID = xray.NewTraceID()
			traceHeader.SamplingDecision = xrayHeader.Sampled
		}
		traceHeader.ParentID = xray.NewSegmentID()
		r.Header.Set(xray.LambdaTraceHeaderKey, traceHeader.String())
	}
	return handler.HandleBuild(ctx, input)
}

func AddAwsXrayTraceHeadersMiddleware(s *smithyMiddleware.Stack) error {
	return s.Build.Add(&addAwsXrayTraceHeadersMiddleware{}, smithyMiddleware.After)
}
