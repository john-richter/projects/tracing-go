package tracing

const (
	LogDDTraceIDKey = "dd.trace_id"
	LogDDSpanIDKey  = "dd.span_id"
)
